import * as React from 'react'
import * as ReactDOM from 'react-dom'
// @ts-ignore
import App from './App'

ReactDOM.render(<App />, document.getElementById("root"))