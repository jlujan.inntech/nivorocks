import React from 'react'
import {MyResponsiveBar} from "./charts/bar";
import {ResponsiveLine} from "@nivo/line";
import {MyResponsiveRadialBar} from "./charts/radial";
import {MyResponsiveChoropleth} from "./charts/choropleth";
import {MyResponsivePie} from "./charts/pie";

// @ts-ignore
import bar from "../helpers/bar.json"
// @ts-ignore
import line from "../helpers/line.json"
// @ts-ignore
import radial from "../helpers/radial.json"
// @ts-ignore
import choropleth from "../helpers/choropleth.json"
// @ts-ignore
import pie from "../helpers/pie.json"

const App: React.FC = () => {
    return (
        <>
            <MyResponsiveRadialBar data={radial}/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <MyResponsiveBar data={bar}/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <MyResponsivePie data={pie}/>
        </>
    )
}
export default App